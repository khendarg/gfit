#!/usr/bin/env python

from Bio import SeqIO
import argparse
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import scipy.stats

HYDROPATHY = {'G':-0.400, \
	'I':4.500, \
	'S':-0.800, \
	'Q':-3.500, \
	'E':-3.500, \
	'A':1.800, \
	'M':1.900, \
	'T':-0.700, \
	'Y':-1.300, \
	'H':-3.200, \
	'V':4.200, \
	'F':2.800, \
	'C':2.500, \
	'W':-0.900, \
	'K':-3.900, \
	'L':3.800, \
	'P':-1.600, \
	'N':-3.500, \
	'D':-3.500, \
	'R':-4.500, \
	'U':0, \
	'B':-3.500, \
	'J':-3.500, \
	'Z':4.150 \
}

def get_continuous_maxima(func, points):
	arr = func(points)
	percent5 = 0.1 * np.max(arr)
	dfdt = np.convolve(arr, [-0.5, 0, 0.5], 'same')
	df2dt2 = np.convolve(arr, [1, -2, 1], 'same')
	dfdt_f1 = np.roll(dfdt, 1)
	return points[(df2dt2 < 0) * (dfdt >= 0) * (dfdt_f1 < 0) * (arr > percent5)]

def nansqueeze(arr):
	arr2 = np.zeros(0)
	nans = []
	for i, x in enumerate(arr):
		if np.isnan(x): nans.append(i)
		else: arr2 = np.hstack([arr2, [x]])
	return arr2, nans

def nanunsqueeze(arr, nans):
	arr2 = np.zeros(0)
	for x in arr:
		if len(arr2) in nans: arr2 = np.hstack([arr2, [np.nan]])
		arr2 = np.hstack([arr2, [x]])
	return arr2

def get_peaks(seq, window=15):
	rawhydro = np.array([HYDROPATHY.get(resn, np.nan) for resn in seq.seq])

	vhydro, nans = nansqueeze(rawhydro)

	b, a = scipy.signal.butter(3, 1/window, btype='lowpass', output='ba')
	filtvhydro = scipy.signal.lfilter(b, a, vhydro)
	kde = scipy.stats.gaussian_kde(np.arange(0, len(filtvhydro)), bw_method=window/len(filtvhydro)/2, weights=np.clip(filtvhydro, 0, 100))
	rawvals = kde(np.arange(0, len(filtvhydro)))
	factor = np.max(filtvhydro) / np.max(rawvals)
	vals = rawvals * factor * (rawvals > 0)

	maxima = get_continuous_maxima(kde.pdf, np.arange(0, len(filtvhydro), 1))
	peaks = kde(maxima) * factor

	for pos in nans: maxima[maxima > pos] += 1

	return maxima, peaks

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('infile', nargs='?', default='/dev/stdin')

	parser.add_argument('-w', '--window', type=float, default=15.0, help='Period cutoff for low pass and KDE')
	parser.add_argument('--print-heights', action='store_true', help='Print peak heights as well')
	#parser.add_argument('--show', action='store_true', help='Show the resulting points')

	args = parser.parse_args()

	#with open(args.infile) as fh:
	seqlist = SeqIO.parse(args.infile, 'fasta')

	for seq in seqlist:
		maxima, peaks = get_peaks(seq, window=args.window)
		if args.print_heights: 
			valstr = ' '.join(['{},{:0.2f}'.format(x,y) for x,y in zip(maxima, peaks)])
		else: valstr = ' '.join([str(x) for x in maxima])

		print('>{} {}aa {}   {}'.format(seq.name, len(seq.seq), len(maxima), valstr))
